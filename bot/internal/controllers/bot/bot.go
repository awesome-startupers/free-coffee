package bot

import (
	"random-coffee/internal/bot"
	"random-coffee/internal/controllers"
	"random-coffee/internal/states"
	storage "random-coffee/internal/states/storage/interface"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type controller struct {
	tgbot   bot.Interface
	storage storage.Interface
}

func New(tgbot bot.Interface, storage storage.Interface) controllers.Interface {
	return &controller{
		tgbot:   tgbot,
		storage: storage,
	}
}

func (controller *controller) HandleUpdate(update tgbotapi.Update) {
	user := update.SentFrom()
	if user == nil {
		return
	}
	state, err := controller.storage.GetState(user.ID)
	if err != nil {
		return
	}

	// command handlers
	if update.Message != nil {
		if len(update.Message.Photo) > 0 {
			if update.Message.Caption == "/"+bot.ChangePhotoCmd {
				controller.changePhotoCmdHandler(user.ID, update)
				return
			}
		}
		if update.Message.IsCommand() {
			switch update.Message.Command() {
			case bot.StartCmd:
				if state == states.NotStartedState {
					controller.notStartedHandler(user.ID, update)
				}
			default:
				controller.defaultCmdHandler(user.ID)
			}
			return
		}
	}

	// state handlers
	switch state {
	case states.NotStartedState:
		controller.notStartedHandler(user.ID, update)
	case states.WaitNameState:
		controller.waitNameHandler(user.ID, update)
	case states.WaitInfoState:
		controller.waitInfoHandler(user.ID, update)
	case states.WaitOrgState:
		controller.waitOrgHandler(user.ID, update)
	case states.WaitOrgPasswordState:
		controller.waitOrgPasswordHandler(user.ID, user.UserName, update)
	case states.WaitParticipationtAskingState:
		controller.waitParticipationAskingHandler(user.ID, update)
	case states.WaitAnswerState:
		controller.waitAnswerHandler(user.ID, update)
	case states.WaitMatchingState:
		controller.waitMatchingHandler(user.ID, update)
	case states.WaitMatchingConfirmationState:
		controller.waitMatchingConfirmationHandler(user.ID, update)
	case states.ErrorState:
		controller.errorHandler(user.ID, update)

	}
}

func (controller *controller) Run() {
	updates := controller.tgbot.GetUpdatesChan()
	for update := range updates {
		controller.HandleUpdate(update)
	}
}
