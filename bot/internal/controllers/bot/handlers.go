package bot

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"random-coffee/internal/bot"
	"random-coffee/internal/config"
	"random-coffee/internal/models"
	"random-coffee/internal/states"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

var ErrInternal = errors.New("Internal Error")
var ErrNotMessage = errors.New("Is not a message")

func (controller *controller) notStartedHandler(userID int64, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")

	log.Println("Start notStartedHandler")
	log.Println("Sending StartText")

	if err := controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.StartText)); err != nil {
		log.Println(err)
		return err
	}

	log.Println("Setting state WaitNameState")

	if err := controller.storage.SetState(userID, states.WaitNameState); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (controller *controller) waitNameHandler(userID int64, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")

	log.Println("Start waitNameHandler")

	if update.Message == nil {
		return ErrNotMessage
	}

	log.Println("Sending WaitInfoText")

	if err := controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.WaitInfoText)); err != nil {
		log.Println(err)
		return err
	}

	log.Println("Setting state WaitInfoState")

	if err := controller.storage.SetState(userID, states.WaitInfoState); err != nil {
		log.Println(err)
		return err
	}

	log.Println("Setting name " + update.Message.Text)

	if err := controller.storage.SetName(userID, update.Message.Text); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (controller *controller) waitInfoHandler(userID int64, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")

	log.Println("Start waitInfoHandler")

	if update.Message == nil {
		return ErrNotMessage
	}

	log.Println("Sending WaitOrgText")
	if err := controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.WaitOrgText)); err != nil {
		log.Println(err)
		return err
	}

	if err := controller.storage.SetState(userID, states.WaitOrgState); err != nil {
		log.Println(err)
		return err
	}

	if err := controller.storage.SetInfo(userID, update.Message.Text); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (controller *controller) waitOrgHandler(userID int64, update tgbotapi.Update) error {
	if update.Message == nil {
		return ErrNotMessage
	}

	if err := controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.WaitOrgPasswordText)); err != nil {
		log.Println(err)
		return err
	}

	if err := controller.storage.SetState(userID, states.WaitOrgPasswordState); err != nil {
		log.Println(err)
		return err
	}

	if err := controller.storage.SetOrg(userID, update.Message.Text); err != nil {
		return err
	}

	return nil
}

func (controller *controller) waitOrgPasswordHandler(userID int64, nick string, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")

	log.Println("Start waitOrgPasswordHandler")

	if update.Message == nil {
		log.Println(ErrNotMessage)
		return ErrNotMessage
	}

	name, err := controller.storage.GetName(userID)
	if err != nil {
		log.Println(err)
		return err
	}

	info, err := controller.storage.GetInfo(userID)
	if err != nil {
		log.Println(err)
		return err
	}

	org, err := controller.storage.GetOrg(userID)
	if err != nil {
		log.Println(err)
		return err
	}

	password := update.Message.Text
	data, err := json.Marshal(models.User{
		ID:       userID,
		Nick:     nick,
		Name:     name,
		Info:     info,
		Org:      org,
		Password: password,
	})
	if err != nil {
		log.Println(err)
		return err
	}

	var nextText string
	var nextState states.State

	if resp, err := http.Post(config.ServerAddr+config.PostRegisterAPI, "application/json", bytes.NewBuffer(data)); err != nil {
		log.Println(err)
		return err
	} else if resp.StatusCode == http.StatusOK {
		nextText = bot.SuccessfulRegistrationText
		nextState = states.WaitParticipationtAskingState
	} else if resp.StatusCode == http.StatusUnauthorized {
		nextText = bot.WrongPasswordText
		nextState = states.WaitOrgState
	} else if resp.StatusCode == http.StatusNotFound {
		nextText = bot.WrongOrgText
		nextState = states.WaitOrgState
	} else {
		log.Println(ErrInternal, resp.StatusCode)
		return ErrInternal
	}

	if err := controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, nextText)); err != nil {
		log.Println(err)
		return err
	}

	if err := controller.storage.SetState(userID, nextState); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (controller *controller) waitParticipationAskingHandler(userID int64, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")
	log.Println("Start waitParticipationAskingHandler")

	return controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.WaitParticipationAskingText))
}

func (controller *controller) waitAnswerHandler(userID int64, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")
	log.Println("Start waitAnswerHandler")

	if update.Message == nil {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bot.WaitAnswerText)

		log.Println("Sending WaitAnswerText")

		msg.ReplyMarkup = bot.YesNoKeyboard

		return controller.tgbot.SendMessage(msg)
	}

	var wantsToParticipate bool
	if update.Message.Text == "Да" {
		log.Println("Answer yes")
		log.Println("Sending WaitMatchingText")

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bot.WaitMatchingText)
		msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)

		log.Println("Setting state WaitMatchingState")

		if err := controller.storage.SetState(userID, states.WaitMatchingState); err != nil {
			log.Println(err)
			return err
		}
		if err := controller.tgbot.SendMessage(msg); err != nil {
			log.Println(err)
			return err
		}

		wantsToParticipate = true
	} else if update.Message.Text == "Нет" {
		log.Println("Answer no")
		log.Println("Sending WaitParticipationAskingText")

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bot.WaitParticipationAskingText)
		msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)

		log.Println("Setting state WaitParticipationtAskingState")

		if err := controller.storage.SetState(userID, states.WaitParticipationtAskingState); err != nil {
			log.Println(err)
			return err
		}
		if err := controller.tgbot.SendMessage(msg); err != nil {
			log.Println(err)
			return err
		}

		wantsToParticipate = false
	} else {
		log.Println("Sending WaitAnswerText")

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bot.WaitAnswerText)
		msg.ReplyMarkup = bot.YesNoKeyboard
		return controller.tgbot.SendMessage(msg)
	}

	data, err := json.Marshal(map[string]interface{}{
		"wantsToParticipate": wantsToParticipate,
	})
	if err != nil {
		log.Println(err)
		return err
	}

	log.Println(map[string]interface{}{
		"wantsToParticipate": wantsToParticipate,
	})

	resp, err := http.Post(config.ServerAddr+fmt.Sprintf(config.PostAnswerAPI, userID), "application/json", bytes.NewBuffer(data))

	log.Println(resp.StatusCode)

	if err != nil {
		log.Println(err)
		return err
	}
	if resp.StatusCode != http.StatusOK {
		return ErrInternal
	}

	return nil
}

func (controller *controller) waitMatchingHandler(userID int64, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")
	log.Println("Start waitMatchingHandler")

	return controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.WaitMatchingText))
}

func (controller *controller) waitMatchingConfirmationHandler(userID int64, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")
	log.Println("Start waitMatchingConfirmationHandler")

	if update.Message == nil {
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bot.WaitMatchingConfirmationText)

		log.Println("Sending WaitMatchingConfirmationText")

		msg.ReplyMarkup = bot.YesNoKeyboard

		return controller.tgbot.SendMessage(msg)
	}

	if update.Message.Text == "Да" {
		log.Println("Answer yes")
		log.Println("Sending ConfirmMatchingText")

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bot.ConfirmMatchingText)
		msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)

		log.Println("Setting state WaitParticipationtAskingState")

		if err := controller.storage.SetState(userID, states.WaitParticipationtAskingState); err != nil {
			log.Println(err)
			return err
		}

		if err := controller.tgbot.SendMessage(msg); err != nil {
			log.Println(err)
			return err
		}
	} else if update.Message.Text == "Нет" {
		log.Println("Answer no")
		log.Println("Sending WaitParticipationAskingText")

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bot.WaitParticipationAskingText)
		msg.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)

		log.Println("Setting state WaitParticipationtAskingState")

		if err := controller.storage.SetState(userID, states.WaitParticipationtAskingState); err != nil {
			log.Println(err)
			return err
		}

		if err := controller.tgbot.SendMessage(msg); err != nil {
			log.Println(err)
			return err
		}

		match, err := controller.storage.GetMatch(userID)

		if err != nil {
			log.Println(err)
			return err
		}

		log.Println("Removing match of user " + fmt.Sprintf("%d", userID))

		if err := controller.storage.RemoveMatch(userID); err != nil {
			log.Println(err)
			return err
		}

		userID2 := match.User2.ID

		if userID2 == userID {
			userID2 = match.User1.ID
		}

		log.Println("Sending RejectMatchingText")

		reject := tgbotapi.NewMessage(userID2, bot.RejectMatchingText)
		reject.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
		if err := controller.tgbot.SendMessage(reject); err != nil {
			log.Println(err)
			return err
		}

		log.Println("Setting state WaitParticipationtAskingState")

		if err := controller.storage.SetState(userID2, states.WaitParticipationtAskingState); err != nil {
			log.Println(err)
			return err
		}

		log.Println("Sending WaitParticipationAskingText")

		if err := controller.tgbot.SendMessage(tgbotapi.NewMessage(userID2, bot.WaitParticipationAskingText)); err != nil {
			log.Println(err)
			return err
		}

		// data, err := json.Marshal(map[string]interface{}{
		// 	"wantsToParticipate": true,
		// })

		// if err != nil {
		// 	log.Println(err)
		// 	return err
		// }

		// resp, err := http.Post(config.ServerAddr+fmt.Sprintf(config.PostAnswerAPI, userID2), "application/json", bytes.NewBuffer(data))

		// log.Println(resp.StatusCode)

		// if err != nil {
		// 	log.Println(err)
		// 	return err
		// }

		// if resp.StatusCode != http.StatusOK {
		// 	return ErrInternal
		// }
	} else {
		log.Println("Sending WaitMatchingAnswerText")

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, bot.WaitMatchingConfirmationText)
		msg.ReplyMarkup = bot.YesNoKeyboard
		return controller.tgbot.SendMessage(msg)
	}

	return nil
}

func (controller *controller) errorHandler(userID int64, update tgbotapi.Update) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")
	log.Println("Start errorHandler")

	return controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.ErrorText))
}
