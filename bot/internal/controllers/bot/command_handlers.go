package bot

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"random-coffee/internal/bot"
	"random-coffee/internal/config"
	"random-coffee/internal/models"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

func (controller *controller) changePhotoCmdHandler(userID int64, update tgbotapi.Update) error {
	if len(update.Message.Photo) == 0 {
		return controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.PhotoUsageText))
	}

	photo, err := controller.tgbot.GetPhoto(update.Message.Photo[len(update.Message.Photo)-1].FileID)
	if err != nil {
		log.Println(err)
		return err
	}

	data, err := json.Marshal(models.Photo{
		UserID: userID,
		Photo:  photo,
	})
	if err != nil {
		log.Println(err)
		return err
	}

	if resp, err := http.Post(config.ServerAddr+config.PostPhotoAPI, "application/json", bytes.NewBuffer(data)); err != nil {
		log.Println(err)
		return err
	} else if resp.StatusCode != http.StatusOK {
		log.Println(ErrInternal, resp.StatusCode)
		return ErrInternal
	}

	if err := controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.PhotoSentText)); err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (controller *controller) defaultCmdHandler(userID int64) error {
	return controller.tgbot.SendMessage(tgbotapi.NewMessage(userID, bot.NoSuchCommandText))
}
