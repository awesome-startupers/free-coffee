package match

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"random-coffee/internal/bot"
	"random-coffee/internal/config"
	"random-coffee/internal/controllers"
	"random-coffee/internal/models"
	"random-coffee/internal/states"
	storage "random-coffee/internal/states/storage/interface"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type controller struct {
	tgbot   bot.Interface
	storage storage.Interface
}

func New(tgbot bot.Interface, storage storage.Interface) controllers.Interface {
	return &controller{
		tgbot:   tgbot,
		storage: storage,
	}
}

func (c *controller) handleUser(user models.User, with models.User, match models.Match) error {
	resp, err := http.Get(fmt.Sprintf(config.ServerAddr+config.GePhotoAPI, with.ID))
	if err != nil {
		return err
	}

	var photo models.Photo
	if err := json.NewDecoder(resp.Body).Decode(&photo); err != nil {
		return err
	}

	var companionMsg tgbotapi.Chattable
	text := fmt.Sprintf(bot.MatchFoundText, with.Name, with.Info, with.Nick)
	if len(photo.Photo) != 0 {
		msg := tgbotapi.NewPhoto(user.ID, tgbotapi.FileBytes{Bytes: photo.Photo})
		msg.Caption = text

		companionMsg = msg
	} else {
		companionMsg = tgbotapi.NewMessage(user.ID, text)
	}

	if err := c.tgbot.SendMessage(companionMsg); err != nil {
		return err
	}

	confirmationMsg := tgbotapi.NewMessage(user.ID, bot.WaitMatchingConfirmationText)
	confirmationMsg.ReplyMarkup = bot.YesNoKeyboard

	if err := c.tgbot.SendMessage(confirmationMsg); err != nil {
		return err
	}

	if err := c.storage.SetState(user.ID, states.WaitMatchingConfirmationState); err != nil {
		return err
	}

	return c.storage.SetMatch(user.ID, match)
}

func (c *controller) handleMatch(match models.Match) error {
	log.Println("-----------------------------------------")
	defer log.Println("-----------------------------------------")

	log.Println("Starting handleMatch")
	log.Println(match.User1)
	log.Println(match.User2)

	if err := c.handleUser(match.User1, match.User2, match); err != nil {
		return err
	}
	if err := c.handleUser(match.User2, match.User1, match); err != nil {
		return err
	}

	data, err := json.Marshal(map[string]interface{}{
		"id": match.ID,
	})
	if err != nil {
		return err
	}

	resp, err := http.Post(config.ServerAddr+fmt.Sprintf(config.PostMatchAPI, match.ID), "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	} else if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("%s, status code: %d", config.PostMatchAPI, resp.StatusCode)
	}
	return nil
}

func (c *controller) Run() {
	for {
		resp, err := http.Get(config.ServerAddr + config.GetMatchAPI)
		if err == nil {
			var list []models.Match
			err := json.NewDecoder(resp.Body).Decode(&list)
			if err == nil {
				for _, match := range list {
					if c.handleMatch(match) != nil {
						log.Println(err)
					}
				}
			}

		}

		time.Sleep(config.BetweenMatchAsking)
	}
}
