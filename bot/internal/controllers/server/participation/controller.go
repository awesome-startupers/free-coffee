package participation

import (
	"encoding/json"
	"fmt"
	"net/http"
	"random-coffee/internal/bot"
	"random-coffee/internal/config"
	"random-coffee/internal/controllers"
	"random-coffee/internal/models"
	"random-coffee/internal/states"
	storage "random-coffee/internal/states/storage/interface"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

type controller struct {
	tgbot   bot.Interface
	storage storage.Interface
}

func New(tgbot bot.Interface, storage storage.Interface) controllers.Interface {
	return &controller{
		tgbot:   tgbot,
		storage: storage,
	}
}

func (c *controller) handleParticipation(userID int64) {
	msg := tgbotapi.NewMessage(userID, bot.WaitAnswerText)
	msg.ReplyMarkup = bot.YesNoKeyboard
	if c.tgbot.SendMessage(msg) != nil {
		return
	}

	if c.storage.SetState(userID, states.WaitAnswerState) != nil {
		return
	}

	resp, err := http.Post(config.ServerAddr+fmt.Sprintf(config.PostParticipationAPI, userID), "application/json", nil)
	if err != nil {
		return
	} else if resp.StatusCode != http.StatusOK {
		return
	}
}

func (c *controller) Run() {
	for {
		resp, err := http.Get(config.ServerAddr + config.GetParticipationAPI)
		if err == nil {
			var list []models.User
			err := json.NewDecoder(resp.Body).Decode(&list)
			if err == nil {
				for _, user := range list {
					c.handleParticipation(user.ID)
				}

			}
		}

		time.Sleep(config.BetweenParticipationAsking)
	}
}
