package config

import "time"

const (
	BotToken = "5481214189:AAEOWHtEb6Sa6v8p7UpYcpQaHEWEwt7yTa4"

	RedisAddr = "localhost:6379"

	ServerAddr = "http://localhost:8080"

	PostRegisterAPI      = "/random-coffee/user/register"
	PostParticipationAPI = "/random-coffee/user/%d/participation/confirm"
	PostAnswerAPI        = "/random-coffee/user/%d/participation/answer"
	PostMatchAPI         = "/random-coffee/match/%d/notified"
	PostPhotoAPI         = "/random-coffee/photo/"
	GetParticipationAPI  = "/random-coffee/user/participation/need-confirmation"
	GetMatchAPI          = "/random-coffee/match/need-notification"
	GePhotoAPI           = "/random-coffee/photo/%d"

	BetweenMatchAsking         = time.Second * 10
	BetweenParticipationAsking = time.Minute
)
