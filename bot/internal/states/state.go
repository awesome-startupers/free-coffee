package states

type State int

const (
	NotStartedState State = iota
	WaitNameState
	WaitInfoState
	WaitOrgState
	WaitOrgPasswordState
	WaitParticipationtAskingState
	WaitAnswerState
	WaitMatchingState
	WaitMatchingConfirmationState
	ErrorState
	TodoState
)
