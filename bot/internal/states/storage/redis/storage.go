package local

import (
	"context"
	"encoding/json"
	"errors"
	"random-coffee/internal/config"
	"random-coffee/internal/states"
	"strconv"

	"github.com/go-redis/redis/v9"
)

type StorageInterface interface {
	GetState(userID int64) (states.State, error)
	SetState(userID int64, state states.State) error
	GetName(userID int64) (string, error)
	SetName(userID int64, name string) error
}

type storage struct {
	users map[int64]states.State
	rdb   *redis.Client
}

func New() StorageInterface {
	rdb := redis.NewClient(&redis.Options{
		Addr:     config.RedisAddr,
		Password: "",
		DB:       0,
	})

	return &storage{
		users: make(map[int64]states.State),
		rdb:   rdb,
	}
}

func (s *storage) GetState(userID int64) (states.State, error) {
	key := strconv.FormatInt(userID, 10)

	res := s.rdb.Get(context.Background(), key)
	if errors.Is(res.Err(), redis.Nil) {
		s.SetState(userID, states.NotStartedState)
		return states.NotStartedState, nil
	} else if res.Err() != nil {
		return states.ErrorState, res.Err()
	}

	var value states.State
	err := json.Unmarshal([]byte(res.Val()), &value)
	if err != nil {
		return states.ErrorState, err
	}

	return value, nil
}

func (s *storage) SetState(userID int64, state states.State) error {
	key := strconv.FormatInt(userID, 10)

	value, err := json.Marshal(state)
	if err != nil {
		return err
	}

	return s.rdb.Set(context.Background(), key, value, 0).Err()
}

func (s *storage) GetName(userID int64) (string, error) {
	key := "name-" + strconv.FormatInt(userID, 10)

	res := s.rdb.Get(context.Background(), key)
	if res.Err() != nil {
		return "", res.Err()
	}

	return res.Val(), nil
}

func (s *storage) SetName(userID int64, name string) error {
	key := "name-" + strconv.FormatInt(userID, 10)

	return s.rdb.Set(context.Background(), key, []byte(name), 0).Err()
}
