package storage

import (
	"random-coffee/internal/models"
	"random-coffee/internal/states"
)

type Interface interface {
	GetState(userID int64) (states.State, error)
	SetState(userID int64, state states.State) error
	GetName(userID int64) (string, error)
	SetName(userID int64, name string) error
	SetMatch(userID int64, match models.Match) error
	GetMatch(userID int64) (models.Match, error)
	RemoveMatch(userID int64) error
	GetInfo(userID int64) (string, error)
	SetInfo(userID int64, info string) error
	GetOrg(userID int64) (string, error)
	SetOrg(userID int64, org string) error
}
