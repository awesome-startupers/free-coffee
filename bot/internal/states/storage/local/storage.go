package local

import (
	"errors"
	"random-coffee/internal/models"
	"random-coffee/internal/states"
	storage "random-coffee/internal/states/storage/interface"
)

type localStorage struct {
	users   map[int64]states.State
	names   map[int64]string
	matches map[int64]models.Match
	infos   map[int64]string
	orgs    map[int64]string
}

func New() storage.Interface {
	return &localStorage{
		users:   make(map[int64]states.State),
		names:   make(map[int64]string),
		matches: make(map[int64]models.Match),
		infos:   make(map[int64]string),
		orgs:    make(map[int64]string),
	}
}

func (s *localStorage) GetState(userID int64) (states.State, error) {
	if res, ok := s.users[userID]; ok {
		return res, nil
	}

	s.users[userID] = states.NotStartedState
	return states.NotStartedState, nil
}

func (s *localStorage) SetState(userID int64, state states.State) error {
	s.users[userID] = state

	return nil
}

func (s *localStorage) GetName(userID int64) (string, error) {
	res, ok := s.names[userID]
	if !ok {
		return "", errors.New("No such name")
	}

	return res, nil
}

func (s *localStorage) SetName(userID int64, name string) error {
	s.names[userID] = name

	return nil
}

func (s *localStorage) SetMatch(userID int64, match models.Match) error {
	s.matches[userID] = match

	return nil
}

func (s *localStorage) GetMatch(userID int64) (models.Match, error) {
	res, ok := s.matches[userID]
	if !ok {
		return models.Match{}, errors.New("No matches found")
	}

	return res, nil
}

func (s *localStorage) RemoveMatch(userID int64) error {
	delete(s.matches, userID)

	return nil
}

func (s *localStorage) GetInfo(userID int64) (string, error) {
	res, ok := s.infos[userID]
	if !ok {
		return "", errors.New("No such info")
	}

	return res, nil
}

func (s *localStorage) SetInfo(userID int64, info string) error {
	s.infos[userID] = info

	return nil
}

func (s *localStorage) GetOrg(userID int64) (string, error) {
	res, ok := s.orgs[userID]
	if !ok {
		return "", errors.New("No such org")
	}

	return res, nil
}

func (s *localStorage) SetOrg(userID int64, org string) error {
	s.orgs[userID] = org

	return nil
}
