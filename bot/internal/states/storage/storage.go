package storage

import (
	storage "random-coffee/internal/states/storage/interface"
	"random-coffee/internal/states/storage/local"
)

func New() storage.Interface {
	return local.New()
}
