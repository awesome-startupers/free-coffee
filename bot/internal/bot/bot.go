package bot

import (
	"fmt"
	"io"
	"net/http"
	"random-coffee/internal/config"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/pkg/errors"
)

type Interface interface {
	SendMessage(message tgbotapi.Chattable) error
	GetUpdatesChan() tgbotapi.UpdatesChannel
	GetPhoto(fileID string) ([]byte, error)
}

type tgbot struct {
	token string
	bot   *tgbotapi.BotAPI
}

func New() (Interface, error) {
	bot, err := tgbotapi.NewBotAPI(config.BotToken)
	if err != nil {
		return nil, errors.Wrap(err, "New controller: bot cannot be created ")
	}

	return &tgbot{
		token: config.BotToken,
		bot:   bot,
	}, nil
}

func (b *tgbot) GetPhoto(fileID string) ([]byte, error) {
	url, err := b.bot.GetFileDirectURL(fileID)
	if err != nil {
		return []byte{}, err
	}

	resp, err := http.Get(url)
	if err != nil {
		return []byte{}, err
	} else if resp.StatusCode != http.StatusOK {
		return []byte{}, errors.New(fmt.Sprintf("bad status: %s", resp.Status))
	}
	defer resp.Body.Close()

	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, err
	}

	return bytes, nil
}

func (b *tgbot) SendMessage(message tgbotapi.Chattable) error {
	_, err := b.bot.Send(message)

	return err
}

func (b *tgbot) GetUpdatesChan() tgbotapi.UpdatesChannel {
	return b.bot.GetUpdatesChan(tgbotapi.NewUpdate(0))
}
