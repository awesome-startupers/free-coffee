package bot

import tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"

const (
	PhotoSentText                = "Фото успешно загружено."
	PhotoUsageText               = "Если хотите установить свою фотографию -- воспользуйтесь командой /changePhoto с прикрепленным изображением."
	StartText                    = "Это random-coffee mkn bot. Представьтесь, пожалуйста :) "
	WaitInfoText                 = "Расскажите что-нибудь о себе"
	WaitOrgText                  = "Укажите свою организацию"
	WaitOrgPasswordText          = "Укажите пароль организации"
	WrongOrgText                 = "Такой организации не существует. Попробуйте еще раз. " + WaitOrgText
	WrongPasswordText            = "Пароль неправильный. Попробуйте ещё раз. " + WaitOrgText
	WaitParticipationAskingText  = "Ждите. Когда начнется следующий раунд распределений - мы Вам сообщим."
	SuccessfulRegistrationText   = "Вы успешно зарегистрированы. \n" + PhotoUsageText + "\n" + WaitParticipationAskingText
	WaitAnswerText               = "Пожалуйста, сообщите, будете ли в этот раз участвовать"
	WaitMatchingText             = "Ждите. Когда Вас распределят - мы вам сообщим."
	MatchFoundText               = "Собеседник найден, это %s. \n %s \n\n Телеграмм для связи: @%s\n"
	WaitMatchingConfirmationText = "Вы хотите остаться с этим собеседником?"
	ConfirmMatchingText          = "Вы подтвердили мэтч. Учтите, что собеседник может отказаться."
	RejectMatchingText           = "Cобеседник отказался от мэтча с Вами."
	NoSuchCommandText            = "Такой команды не существует :("
	ErrorText                    = "Что-то пошло не так, напишите, пожалуйста, администраторам."

	ChangePhotoCmd = "changePhoto"
	StartCmd       = "start"
)

var (
	YesNoKeyboard = tgbotapi.NewReplyKeyboard(
		tgbotapi.NewKeyboardButtonRow(
			tgbotapi.NewKeyboardButton("Да"),
			tgbotapi.NewKeyboardButton("Нет"),
		),
	)
)
