package models

type Match struct {
	ID    int64 `json:"id"`
	User1 User  `json:"user1"`
	User2 User  `json:"user2"`
}
