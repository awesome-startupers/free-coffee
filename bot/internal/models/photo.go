package models

type Photo struct {
	UserID int64  `json:"userId"`
	Photo  []byte `json:"photo"`
}
