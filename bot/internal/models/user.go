package models

type User struct {
	ID       int64  `json:"id"`
	Nick     string `json:"nick"`
	Name     string `json:"name"`
	Info     string `json:"info"`
	Org      string `json:"orgKeyword"`
	Password string `json:"orgPincode"`
}
