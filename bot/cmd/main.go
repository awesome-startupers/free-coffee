package main

import (
	"random-coffee/internal/bot"
	bc "random-coffee/internal/controllers/bot"
	mc "random-coffee/internal/controllers/server/match"
	pc "random-coffee/internal/controllers/server/participation"
	"random-coffee/internal/states/storage"
)

func main() {
	statesStorage := storage.New()

	tgbot, err := bot.New()
	if err != nil {
		panic(err)
	}

	botController := bc.New(tgbot, statesStorage)
	participationController := pc.New(tgbot, statesStorage)
	matchController := mc.New(tgbot, statesStorage)

	go participationController.Run()
	go matchController.Run()
	botController.Run()
}
